JCC = javac
JFLAGS = -g

default: HelloWorld.class

Helloworld.class: HelloWorld.java
	$(JCC) $(JFLAGS) HelloWorld.java

clean:
	$(RM) *.class
