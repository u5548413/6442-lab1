import javax.swing.*;
import java.awt.*;

public class HelloWorld {
	public static void main(String[] args)
	{
		Font font = new Font("Courier New",Font.PLAIN,70);
		UIManager.put("Button.font",font);
		JFrame mw = new JFrame("Lab1");
		mw.setSize(800,450);
		JButton button = new JButton("Hello Worrrrrrld!");
		mw.getContentPane().add(button);
		mw.setVisible(true);
	}
}
